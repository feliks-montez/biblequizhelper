class CreateGrids < ActiveRecord::Migration
  def change
    create_table :grids do |t|
      t.string :name
      t.text :body
      t.string :question_type
      t.date :date

      t.timestamps null: false
    end
  end
end
