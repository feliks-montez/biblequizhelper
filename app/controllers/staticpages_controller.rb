class StaticpagesController < ApplicationController
  def index
    render layout: false
  end
end
